# Introduction à Bootstrap 5

Correction du TP Technologies du WEb '[Introduction au framework Bootstrap 5](https://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/AI/sujets/tp04_Bootstrap/index.html)' 

Ce repository git permet de suivre l'évolution du TP exercice par exercice.
A chaque version est associée un tag  de la forme **vx.y** où :

- le numéro de version majeur (**x**) indique le numéro de l'exercice traité
- le numéro de version mineur (**y**) indique la numéro de la question dans l'exercice si l'exercice comporte plusieurs questions

Ces tags identifient certains commits particuliers dans l'historique des commits du projet.  Ils vous permettent ainsi
 de récupérer facilement le code correspondant à l'état du projet à la fin de chaque exercice.

Cette version (v6.3) correspond à la solution complète du TP, c'est à dire le projet après la résolution du dernier exercice (question 3 de l'exercice 6).  

Pour avoir le détail des différentes versions utilisez la commande

```bash
git tag -l -n
```
 Les différentes tags sont :

| N° version | description |
|------------|-------------|
| v0         |   v0 : état initial du site - html brut sans CSS |
| v1.0       |     correction exercice 1 : utilisation de la feuille de styles bootsrap 5 |
| v2.1       |     correction exercice 2.1 : ajout d'un container |
| v2.2       |     correction exercice 2.2 : ajout d'une feuille de styles spécifique |
| v2.3       |     correction exercice 2.3 : container-fluid sans marges automatiques |
| v3.1       |     correction exercice 3.1 : les 3 articles sur un row et les 2 sections sur un autre row |
| v3.2       |     correction exercice 3.2 : création d'une 2ème page où les 3 articles occupent respectivement 25%,50% et 25% de la largeur de la fenêtre |
| v3.3       |     correction exercice 3.3 : ajout d'une troisième page avec les 3 premiers paragraphes du deuxième article alignés sur une ligne |
| v4.1       |     correction exercice 4.1 : ajout de l'image campus de Grenoble dans l'article 1 |
| v4.2       |     correction exercice 4.2 : rendre l'image du campus de Grenoble adaptative (fluide) |
| v4.3       |     correction exercice 4.3 :ajout d'images avec effet thumbnail et circulaire |
| v5.1       |     correction exercice 5.1 : ajout d'une barre de navigation aux 3 pages du site |
| v5.2       |     correction exercice 5.2 : rendre les barres de navigation collapsables |
| v5.3       |     corection exercice 5.3: barres de navigations fixes en haut de la page |
| v5.4       |     correction exercice 5.4 : ajout d'un logo UGA dans la barre de navigation | 
| v5.5       |     correction exercice 5.5 : ajout d'un menu dropdown pour la page trois permettant d'accéder directement aux articles ou aux sections |
| v6.1       |     correction exercice 6.1 : ajout d'une nouvelle page (maPage4.html) contenant un tableau |
| v6.2       |     correction exercice 6.2 : mise en forme simple du tableau HTML avec bootstrap |
| v6.3       |     correction de l'exercice 6.3 : mise en forme avancée du tableau HTML avec bootstrap |


Pour récupérer le code source correspondant à une version donnée 

```bash
git checkout -b exercice.x.y v.x.y
```

qui permet de créer une branche de nom `exercice.x.y` en récupérant le code de la version `vx.y`. Par exemple

```bash
git checkout -b exercice.5.2 v5.2
```

vous permet de récupérer dans la branche `exercice.5.2` le code de la version `v5.2`. **Attention** une fois cette commande effectuée, vous êtes positionné sur la branche `exercice5.2` (votre répertoire de travail (*working directory*) contient les fichiers correspondant au commit identifié par le tag `v5.2`). Vous pouvez revenir sur la branche `master` à l'aide de la commande

```bash
git checkout master
```

Vous pouvez alors (si vous le souhaitez) détruire la branche `exercice.5.2` par la commande 

```bash
git branch -d exercice.5.2
```

Vous pouvez aussi à l'aide de la commande `git diff` visualiser facilement les différences entre deux versions d'un code. Par exemple

```bash
git diff v5.3 v5.2  maPage4.html
```
vous montrera les différences entre la version `v5.3` et la version `v5.2` du code HTML du fichier  `maPage4.html`

